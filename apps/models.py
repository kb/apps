# -*- encoding: utf-8 -*-
import collections
import operator

from django.apps import apps
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models
from django.db.models import Q
from django.urls import reverse
from functools import reduce
from reversion import revisions as reversion

from base.singleton import SingletonModel
from base.model_utils import (
    TimedCreateModifyDeleteModel,
    TimedCreateModifyDeleteArchiveModel,
)


def current_users():
    contact_model = get_contact_model()
    user_pks = contact_model.objects.current().values_list(
        "user__pk", flat=True
    )
    return get_user_model().objects.filter(pk__in=user_pks)


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


class AppError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class AppManager(models.Manager):
    """App manager.

    .. tip:: Check https://www.kbsoftware.co.uk/docs/app-apps.html for docs.

    """

    def _count_apps(self, apps):
        """Number of apps where the user has access."""
        result = 0
        for _, v in apps.items():
            if v["access"] is True:
                result = result + 1
        return result

    def _current_administrator(self, user):
        """List of apps where I am an administrator."""
        return self.current().filter(administrators__in=[user])

    def admin_apps(self, user):
        """List of apps where I am an administrator."""
        if AppSettings.objects.is_app_superuser(user):
            qs = self.current()
        else:
            qs = self._current_administrator(user)
        return qs

    def admin_app_groups(self, user):
        """List of apps where the user is a group administrator.

        .. note:: We hide apps where ``hide_app_groups`` is ``True`` e.g. QMS
                  does not use application groups so we hide the group edit menu
                  item.

        """
        return self.admin_apps(user).exclude(hide_app_groups=True)

    def app_administrators(self, slug):
        """App administrators (does not include superusers)."""
        app = self.model.objects.get(slug=slug)
        return app.administrators.exclude(is_active=False)

    def app_menu(self, current_app_slug, user):
        current_app = None
        current_app_admin = show_app_menu = show_settings = False
        if current_app_slug:
            try:
                current_app = App.objects.get(slug=current_app_slug)
            except self.model.DoesNotExist:
                raise AppError(
                    "Cannot find an 'App' with a slug of '{}'".format(
                        current_app_slug
                    )
                )
        if user.is_authenticated:
            user_apps = self.user_apps(user)
            user_apps_count = user_apps.count()
            # we only have access to one app, so we can set the current app
            if current_app is None and user_apps_count == 1:
                current_app = user_apps.first()
            if current_app:
                current_app_admin = current_app.administrators.filter(
                    pk__in=[user.pk]
                ).exists()
                if not show_settings:
                    show_settings = current_app_admin
            show_app_menu = user_apps_count > 1
            if not show_settings:
                show_settings = App.objects.is_administrator_for_any_app(user)
            if not show_settings:
                show_settings = AppSettings.objects.is_contact_administrator(
                    user
                )
        return {
            "current_app": current_app,
            "current_app_admin": current_app_admin,
            "show_app_menu": show_app_menu,
            "show_settings": show_settings,
        }

    def app_perms(self, user):
        """Which app permissions does the user have?

        The method will return a dictionary in this format::

          {
              'accounts': {
                  'access': True,
                  'admin': True,
              },
              'crm': {
                  'access': True,
                  'admin': False,
              },
              "app_superuser": False,
              "contact_administrator": False,
              "show_app_menu": True,
              "show_settings": True,
          }

        .. note:: We use a ``defaultdict`` so we can return a default value
                  (``is_superuser``) for missing apps.

        If we return the result in the context of a view::

          app_perms=App.objects.app_perms_for_template(self.request.user),

        We can check to see if the user is an admin::

          {% if app_perms.myapp.admin %}

        We can check to see if the user has acccess to the app::

          {% if app_perms.myapp.access %}

        We can check to see if the user is an application superuser::

          {% if app_perms.app_superuser %}

        We can check to see if the settings menu should be displayed::

          {% if app_perms.show_settings %}

        """
        app_administrator = contact_admin = is_superuser = show_settings = False
        if user.is_authenticated:
            contact_admin = AppSettings.objects.is_contact_administrator(user)
            is_superuser = AppSettings.objects.is_app_superuser(user)
        result = collections.defaultdict(
            lambda: dict(admin=is_superuser, access=is_superuser)
        )
        if user.is_authenticated and not is_superuser:
            for app in self._current_administrator(user):
                # an admin will have access
                result[app.slug]["access"] = True
                result[app.slug]["admin"] = True
                app_administrator = show_settings = True
            for app in self.user_apps(user):
                result[app.slug]["access"] = True
        # note: count apps before adding other keys to the result
        app_count = self._count_apps(result)
        # note: update 'keys_to_check' in 'create_app' if you change them here:
        result["app_administrator"] = is_superuser or app_administrator
        result["app_superuser"] = is_superuser
        result["contact_administrator"] = contact_admin
        result["show_app_menu"] = bool(app_count > 1)
        result["show_settings"] = contact_admin or is_superuser or show_settings
        return result

    def create_app(
        self,
        slug,
        name,
        menu,
        icon,
        hide_app_groups,
        url_name_contact_edit,
        url_dash,
        open_in_new_tab,
    ):
        """Create a new app.

        .. note:: ``keys_to_check`` are not allowed in the slug because they are
                  used as keys in the ``dict`` returned from ``app_perms``.

        """
        keys_to_check = (
            "app_administrator",
            "app_superuser",
            "contact_administrator",
            "show_app_menu",
            "show_settings",
        )
        for x in keys_to_check:
            if x in slug:
                raise AppError(
                    "The slug for the app ('{}') cannot contain {}".format(
                        slug, keys_to_check
                    )
                )
        app = self.model(
            slug=slug,
            name=name,
            menu=menu,
            icon=icon,
            hide_app_groups=hide_app_groups,
            url_name_contact_edit=url_name_contact_edit,
            url_dash=url_dash,
            open_in_new_tab=open_in_new_tab,
        )
        app.save()
        return app

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def is_administrator(self, slug, user):
        """Is this user an administrator for the app?

        We check:

        1. Is the user an administrator for the app (identified by the ``slug``)
        2. Is the user an app superuser (``AppSettings.app_superuser``)
        3. Is the user a Django superuser.

        .. note:: It feels like this method should belong in the ``App`` class
                  but it is used by the ``DocRecordFolder`` permissions.  If
                  we move the class to ``App``, then it makes more SQL queries.

        """
        result = False
        if slug:
            qs = (
                self.current()
                .filter(slug=slug)
                .filter(administrators__in=[user])
            )
            result = qs.exists()
        if not result:
            result = AppSettings.objects.is_app_superuser(user)
        return result

    def is_administrator_for_any_app(self, user):
        """Is this user an administrator for any app?"""
        qs = self.current().filter(administrators__in=[user])
        result = qs.exists()
        if not result:
            result = AppSettings.objects.is_app_superuser(user)
        return result

    def has_access(self, user, slug):
        user_apps = self.user_apps(user).values_list("slug", flat=True)
        return slug in user_apps

    def user_apps(self, user):
        """Apps available to this user.

        .. note:: For a very similar method, see ``_user_folders`` in the
                  ``docrecord`` app.

        """
        if AppSettings.objects.is_app_superuser(user):
            qs = App.objects.current()
        else:
            app_groups_for_user = AppGroup.objects.app_groups_for_user(user)
            q = Q()
            q |= Q(administrators__in=[user])
            q |= Q(app_group__in=app_groups_for_user)
            q |= Q(global_group__in=user.groups.all())
            qs = self.model.objects.current().filter(q)
            qs = qs.distinct("slug").order_by("slug")
        return qs

    def user_dash_url(self, user):
        """Dashboard URL for this user."""
        result = self.user_dash_url_if_one(user)
        if not result:
            result = reverse("apps.list")
        return result

    def user_dash_url_if_one(self, user):
        """Dashboard URL for this user (if there is a single one)."""
        result = None
        user_apps = self.user_apps(user)
        count = user_apps.count()
        if count == 1:
            app = user_apps.first()
            result = app.url_dash
        return result


class App(TimedCreateModifyDeleteModel):
    """Applications e.g. reporting.

    .. tip:: Check https://www.kbsoftware.co.uk/docs/app-apps.html for docs.

    """

    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=255)
    menu = models.CharField(
        max_length=20, help_text="Menu caption (to fit in the left hand menu)"
    )
    icon = models.CharField(
        max_length=30,
        help_text=(
            "Choose an icon from http://fontawesome.io/icons/ "
            "e.g. 'fa-adjust'"
        ),
    )
    open_in_new_tab = models.BooleanField(default=False)
    administrators = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        help_text="Application administrators",
        related_name="app_administrators",
    )
    app_group = models.ManyToManyField(
        "apps.AppGroup",
        blank=True,
        related_name="app_group",
        help_text=(
            "Users belonging to these groups have permission to access the app"
        ),
    )
    global_group = models.ManyToManyField(
        Group,
        blank=True,
        related_name="group",
        help_text=(
            "Users belonging to these groups have permission to access the app"
        ),
    )
    hide_app_groups = models.BooleanField(default=False)
    url_dash = models.URLField(help_text="URL for application landing page")
    # This is a Django URL name because it will be used in the settings
    # menu to edit a contact e.g:
    # <a href="{% url app.url_name_contact_edit object.pk %}">
    url_name_contact_edit = models.CharField(
        max_length=200, help_text="Django URL name for editing a contact"
    )
    objects = AppManager()

    class Meta:
        ordering = ["name"]
        verbose_name = "Application"
        verbose_name_plural = "Applications"

    def __str__(self):
        return "{}".format(self.slug)

    def can_edit_contact(self):
        return bool(self.url_name_contact_edit)


reversion.register(App)


class AppGroupManager(models.Manager):
    def app_groups(self, app):
        return self.model.objects.exclude(deleted=True).filter(app=app)

    def app_groups_for_app_user(self, app_slug, user):
        qs = self.model.objects.current().filter(app__slug=app_slug)
        q = Q(appgroupuser__user=user) & ~Q(appgroupuser__deleted=True)
        return qs.filter(q)

    def app_groups_for_user(self, user):
        qs = self.model.objects.current()
        q = Q(appgroupuser__user=user) & ~Q(appgroupuser__deleted=True)
        return qs.filter(q)

    def create_app_group(self, app, name):
        obj = self.model(app=app, name=name)
        obj.save()
        return obj

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def init_app_group(self, app, name):
        try:
            obj = self.model.objects.get(app=app, name=name)
        except self.model.DoesNotExist:
            obj = self.create_app_group(app, name)
        return obj

    def search(self, free_text):
        """Generate queries for each word in the input string.

        e.g "pet lamb" would result the following query set::

          (
            Q(name__icontains='pet')
            & Q(name__icontains='lamb')
          )

        """
        q = Q(
            # this checks if the name contains all the words
            reduce(
                operator.and_,
                (Q(name__icontains=x) for x in free_text.split(" ")),
            )
        )

        return self.model.objects.filter(q)


class AppGroup(TimedCreateModifyDeleteArchiveModel):
    """Application groups.

    Each application e.g. DEDA or QMS can have it's own set of groups.  Django
    groups are at a level above this.

    .. tip:: Check https://www.kbsoftware.co.uk/docs/app-apps.html for docs.

    .. note:: For now, QMS uses category groups rather than application groups.


    .. warning:: PJK, 11/06/2023, I have no idea why we have an ``app`` here
                 because the the ``app_group`` field in the ``App`` model
                 keeps track of the app for the group.

    """

    app = models.ForeignKey(App, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=True)
    objects = AppGroupManager()

    class Meta:
        ordering = ("app__name", "name")
        verbose_name = "App Group"
        verbose_name_plural = "App Groups"

    def __str__(self):
        return "{} ({})".format(self.name, self.app.name)


reversion.register(AppGroup)


class AppGroupUserManager(models.Manager):
    def app_groups(self, user):
        return self.current().filter(user=user)

    def create_app_group_user(self, app_group, user):
        obj = self.model(app_group=app_group, user=user)
        obj.save()
        return obj

    def current(self):
        """Exclude deleted contacts.

        https://www.kbsoftware.co.uk/crm/ticket/5644/

        """
        user_pks = (
            get_contact_model()
            .objects.current()
            .values_list("user__pk", flat=True)
        )
        return self.model.objects.exclude(deleted=True).filter(
            user__pk__in=user_pks
        )

    def init_app_group_user(self, app_group, user):
        """

        This method is in ``AppGroupUserManager``.

        """
        try:
            x = self.model.objects.get(app_group=app_group, user=user)
            if x.is_deleted:
                x.undelete()
        except self.model.DoesNotExist:
            x = self.create_app_group_user(app_group, user)
        return x

    def remove_user_from_app_group(self, user, app_group, user_deleted):
        try:
            x = self.model.objects.get(app_group=app_group, user=user)
            x.set_deleted(user_deleted)
        except self.model.DoesNotExist:
            pass

    def update_app_group_user(self, app, app_groups, user):
        for group in app_groups:
            self.init_app_group_user(group, user)
        self.model.objects.filter(app_group__app=app).filter(user=user).exclude(
            app_group__in=app_groups
        ).delete()

    def user_groups(self, app, user):
        return self.current().filter(app_group__app=app).filter(user=user)

    def users(self, app_group):
        """List of 'AppGroupUser' objects for a group."""
        return self.current().filter(app_group=app_group)

    def users_in_groups(self, app_groups):
        """List of 'AppGroupUser' objects for a list of app groups."""
        return self.current().filter(app_group__in=app_groups)


class AppGroupUser(TimedCreateModifyDeleteModel):
    """Users for an application group.

    .. tip:: Check https://www.kbsoftware.co.uk/docs/app-apps.html for docs.

    """

    app_group = models.ForeignKey(AppGroup, on_delete=models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="app_group_user",
        on_delete=models.CASCADE,
    )
    objects = AppGroupUserManager()

    class Meta:
        ordering = ("app_group__name", "user__username")
        unique_together = ["app_group", "user"]
        verbose_name = "App Group User"
        verbose_name_plural = "App Group Users"

    def __str__(self):
        return "{}: {}".format(self.app_group.name, self.user.username)


reversion.register(AppGroupUser)


class AppSettingsManager(models.Manager):
    def is_app_superuser(self, user):
        result = False
        if user.is_superuser:
            result = True
        else:
            result = self.user_is_app_superuser(user)
        return result

    def is_contact_administrator(self, user):
        result = False
        if user.is_superuser:
            result = True
        else:
            result = self.user_is_contact_administrator(user)
        return result

    def user_is_app_superuser(self, user):
        """Is this user an application superuser?"""
        qs = self.model.objects.filter(app_superusers__in=[user])
        return qs.exists()

    def user_is_contact_administrator(self, user):
        """Is this user a contact administrator?"""
        qs = self.model.objects.filter(contact_administrators__in=[user])
        return qs.exists()


class AppSettings(SingletonModel):
    """App superuser (can set administrators on an app).

    .. tip:: Check https://www.kbsoftware.co.uk/docs/app-apps.html for docs.

    """

    app_superusers = models.ManyToManyField(
        settings.AUTH_USER_MODEL, blank=True, related_name="app_superusers"
    )
    contact_administrators = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="contact_administrators",
    )
    objects = AppSettingsManager()

    class Meta:
        verbose_name = "App Settings"

    def __str__(self):
        """String representation of the application settings instance.

        .. note:: We need to check ``self.pk`` in case the model was created
                  using ``load`` method (from the ``SingletonModel``).

        """
        x = "App superusers {} Contact administrators {}"
        if self.pk:
            return x.format(
                [x.username for x in self.app_superusers.all()],
                [x.username for x in self.contact_administrators.all()],
            )
        else:
            return x.format([], [])


reversion.register(AppSettings)
