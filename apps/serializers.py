# -*- encoding: utf-8 -*-
from rest_framework_json_api import serializers

from apps.models import App, AppGroup, AppGroupUser


class AppSerializer(serializers.ModelSerializer):
    class Meta:
        model = App
        fields = [
            "id",
            "slug",
            "name",
            "menu",
            "icon",
            "url_dash",
            "open_in_new_tab",
        ]


class AppGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppGroup
        fields = ("id", "app", "name")

    @staticmethod
    def setup_eager_loading(queryset):
        return queryset.select_related("app")

    def get_app(self, app_group):
        rtn = ""
        if app_group.app:
            serializer = AppSerializer(instance=app_group.app, many=False)
            rtn = serializer.data
        return rtn


# class AppGroupUserSerializerV1(serializers.ModelSerializer):
#    class Meta:
#        model = AppGroupUser
#        fields = ("id", "app_group", "user")
#
#    @staticmethod
#    def setup_eager_loading(queryset):
#        return queryset.select_related("app_group", "user")


class AppGroupUserSerializerV2(serializers.ModelSerializer):
    user_full_name = serializers.SerializerMethodField()

    class Meta:
        model = AppGroupUser
        fields = ("id", "app_group", "user_full_name")

    def get_user_full_name(self, instance):
        return instance.user.get_full_name()

    @staticmethod
    def setup_eager_loading(queryset):
        return queryset.select_related("app_group", "user")
