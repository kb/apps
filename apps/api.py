# -*- encoding: utf-8 -*-
from rest_framework.authentication import (
    TokenAuthentication,
    SessionAuthentication,
)
from rest_framework.permissions import IsAuthenticated

from api.api_utils import SoftDeleteViewSet
from .filters import AppGroupFilter, AppGroupUserFilter
from .models import App, AppGroup, AppGroupUser
from .serializers import (
    AppGroupSerializer,
    AppGroupUserSerializerV2,
    AppSerializer,
)


class AppViewSet(SoftDeleteViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = AppSerializer

    def get_queryset(self):
        slug = self.request.GET.get("slug")
        if slug:
            return App.objects.filter(slug=slug)
        return App.objects.user_apps(self.request.user)


class AppGroupUserViewSet(SoftDeleteViewSet):
    authentication_classes = (TokenAuthentication,)
    filterset_class = AppGroupUserFilter
    permission_classes = (IsAuthenticated,)
    serializer_class = AppGroupUserSerializerV2

    def get_queryset(self):
        qs = AppGroupUser.objects.all()
        app_group_id = self.request.GET.get("group_id", None)
        if app_group_id:
            qs = qs.filter(app_group__pk=app_group_id)
        return qs


class AppGroupViewSet(SoftDeleteViewSet):
    authentication_classes = (TokenAuthentication,)
    filterset_class = AppGroupFilter
    permission_classes = (IsAuthenticated,)
    serializer_class = AppGroupSerializer

    def get_queryset(self):
        app_id = self.request.GET.get("app", None)
        slug = self.request.GET.get("slug", None)
        if app_id:
            qs = AppGroup.objects.filter(app__id=app_id)
        elif slug:
            qs = AppGroup.objects.filter(app__slug=slug)
        else:
            qs = AppGroup.objects.all()
        return AppGroupSerializer.setup_eager_loading(qs)
