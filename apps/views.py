# -*- encoding: utf-8 -*-
from braces.views import (
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    UserPassesTestMixin,
)
from django.db.models.functions import Lower
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView, UpdateView

from base.view_utils import BaseMixin
from apps.forms import (
    AppAdminForm,
    AppGroupEmptyForm,
    AppGroupForm,
    AppSettingsForm,
    AppSuperForm,
    GroupForm,
)
from apps.models import App, AppGroup, AppGroupUser, AppSettings


def is_contact_or_app_admin(user):
    """Is the user a contact or application administrator?

    Can be used for checking permissions on a view::

      class MyView(
          LoginRequiredMixin, UserPassesTestMixin, BaseMixin, TemplateView

          def test_func(self, user):
              return _is_contact_or_app_admin(user)

    """
    result = AppSettings.objects.is_contact_administrator(user)
    if not result:
        result = App.objects.is_administrator_for_any_app(user)
    return result


class AppAdminUpdateView(
    LoginRequiredMixin, UserPassesTestMixin, BaseMixin, UpdateView
):
    """The application administrator updates the app model.

    Also see ``AppSuperUpdateView`` which updates other fields in the same model

    """

    model = App
    form_class = AppAdminForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(display_app_data=True))
        return context

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        result.update(dict(app=self.object))
        return result

    def get_success_url(self):
        return reverse("project.settings")

    def test_func(self, user):
        app = self.get_object()
        return App.objects.is_administrator(app.slug, self.request.user)


class AppGroupMixin(UserPassesTestMixin):
    model = AppGroup

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(app=self._app()))
        return context

    def test_func(self, user):
        app = self._app()
        return App.objects.is_administrator(app.slug, self.request.user)


class AppGroupCreateMixin(AppGroupMixin):
    def _app(self):
        return App.objects.get(slug=self._app_slug())

    def _app_slug(self):
        return self.kwargs["slug"]


class AppGroupUpdateMixin(AppGroupMixin):
    def _app(self):
        app_group = self.get_object()
        return app_group.app


class AppGroupCreateView(
    LoginRequiredMixin, AppGroupCreateMixin, BaseMixin, CreateView
):
    form_class = AppGroupForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.app = self._app()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("apps.group.list", args=[self.object.app.slug])


class AppGroupDeleteView(
    LoginRequiredMixin, AppGroupUpdateMixin, BaseMixin, UpdateView
):
    model = AppGroup
    form_class = AppGroupEmptyForm
    template_name = "apps/app_group_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("apps.group.list", args=[self.object.app.slug])


class AppGroupDetailView(LoginRequiredMixin, AppGroupMixin, DetailView):
    model = AppGroup

    def _app(self):
        app_group = self.get_object()
        return app_group.app

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(app_group_users=AppGroupUser.objects.users(self.object))
        )
        return context


class AppGroupListView(
    LoginRequiredMixin, AppGroupCreateMixin, BaseMixin, ListView
):
    model = AppGroup

    def get_queryset(self):
        return AppGroup.objects.app_groups(self._app()).order_by(Lower("name"))


class AppGroupUpdateView(
    LoginRequiredMixin, AppGroupUpdateMixin, BaseMixin, UpdateView
):
    model = AppGroup
    form_class = AppGroupForm

    def get_success_url(self):
        return reverse("apps.group.list", args=[self.object.app.slug])


class AppListView(LoginRequiredMixin, BaseMixin, ListView):
    model = App

    def get_queryset(self):
        return App.objects.user_apps(self.request.user)


class AppSettingsUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = AppSettings
    form_class = AppSettingsForm

    def get_object(self):
        return AppSettings.load()

    def get_success_url(self):
        return reverse("project.settings")


class AppSuperUpdateView(
    LoginRequiredMixin, UserPassesTestMixin, BaseMixin, UpdateView
):
    """The application superuser updates the app model.

    .. note:: Also see ``AppAdminUpdateView`` which updates other fields in the
              same model.

    """

    model = App
    form_class = AppSuperForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(display_app_data=False))
        return context

    def get_success_url(self):
        return reverse("project.settings")

    def test_func(self, user):
        """Is the user a superuser or application superuser?"""
        return AppSettings.objects.is_app_superuser(self.request.user)


class GroupMixin(UserPassesTestMixin):
    model = Group

    def test_func(self, user):
        """Only a contact administrator can edit groups."""
        return AppSettings.objects.is_contact_administrator(self.request.user)


class GroupCreateView(LoginRequiredMixin, GroupMixin, BaseMixin, CreateView):
    form_class = GroupForm
    template_name = "apps/group_form.html"

    def get_success_url(self):
        return reverse("group.list")


class GroupDetailListView(LoginRequiredMixin, GroupMixin, BaseMixin, ListView):
    """Group detail view.

    We use a ``ListView`` because a group can have many users.

    """

    paginate_by = 20
    template_name = "apps/group_detail.html"

    def _group(self):
        pk = self.kwargs["pk"]
        return Group.objects.get(pk=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(group=self._group()))
        return context

    def get_queryset(self):
        group = self._group()
        return group.user_set.all().order_by("username")


class GroupListView(LoginRequiredMixin, GroupMixin, BaseMixin, ListView):
    template_name = "apps/group_list.html"

    def get_queryset(self):
        return Group.objects.all().order_by(Lower("name"))


class GroupUpdateView(LoginRequiredMixin, GroupMixin, BaseMixin, UpdateView):
    form_class = GroupForm
    template_name = "apps/group_form.html"

    def get_success_url(self):
        return reverse("group.detail", args=[self.object.pk])


class SettingsBaseMixin(BaseMixin):
    """Information needed for the *Settings* view.

    The view has no 'current_app_slug', but we still want the settings menu to
    display if the user is an app administrator.

    """

    current_app_slug = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        app_superuser = AppSettings.objects.is_app_superuser(self.request.user)
        is_contact_administrator = AppSettings.objects.is_contact_administrator(
            self.request.user
        )
        context.update(
            dict(
                admin_apps=App.objects.admin_apps(self.request.user),
                is_contact_administrator=is_contact_administrator,
                superuser_apps=App.objects.current() if app_superuser else [],
            )
        )
        return context
