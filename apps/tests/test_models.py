# -*- encoding: utf-8 -*-
import pytest

from contact.tests.factories import ContactFactory
from login.tests.factories import UserFactory
from apps.models import current_users


@pytest.mark.django_db
def test_current_users(django_assert_num_queries):
    qs = current_users()
    user_1 = UserFactory(username="u1")
    user_2 = UserFactory(username="u2")
    user_3 = UserFactory(username="u3")
    contact_1 = ContactFactory(user=user_1)
    contact_2 = ContactFactory(user=user_2)
    contact_3 = ContactFactory(user=user_3)
    contact_2.set_deleted(user_2)
    with django_assert_num_queries(1):
        result = [x.username for x in current_users()]
    assert ["u1", "u3"] == result
