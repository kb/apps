# -*- encoding: utf-8 -*-
import pytest

from django.contrib.auth.models import AnonymousUser
from django.urls import reverse

from apps.models import App, AppError
from apps.tests.factories import (
    AppFactory,
    AppGroupFactory,
    AppGroupUserFactory,
    AppSettingsFactory,
)
from example_apps.models import MyApps
from login.tests.factories import GroupFactory, UserFactory


@pytest.mark.django_db
def test_admin_apps():
    user = UserFactory()
    app_1 = AppFactory(slug="Aa")
    app_1.administrators.add(user)
    app_2 = AppFactory(slug="Bb")
    app_2.administrators.add(user)
    app_2.set_deleted(UserFactory())
    assert ["Aa"] == [x.slug for x in App.objects.admin_apps(user)]


@pytest.mark.django_db
def test_admin_apps_app_superuser():
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    app_1 = AppFactory(slug="Aa")
    app_1.set_deleted(UserFactory())
    AppFactory(slug="Bb")
    assert ["Bb"] == [x.slug for x in App.objects.admin_apps(user)]


@pytest.mark.django_db
def test_app_administrators():
    app = AppFactory(slug=MyApps.CRM)
    admin_1 = UserFactory(email="admin-1@test.com")
    app.administrators.add(admin_1)
    UserFactory(email="admin-2@test.com")
    admin_3 = UserFactory(email="admin-3@test.com", is_active=False)
    app.administrators.add(admin_3)
    admin_4 = UserFactory(email="admin-4@test.com")
    app.administrators.add(admin_4)
    assert ["admin-1@test.com", "admin-4@test.com"] == [
        x.email
        for x in App.objects.app_administrators(MyApps.CRM).order_by("email")
    ]


@pytest.mark.django_db
def test_app_menu(django_assert_num_queries):
    user = UserFactory()
    group = GroupFactory()
    user.groups.add(group)
    app = AppFactory()
    app.global_group.add(group)
    with django_assert_num_queries(7):
        result = App.objects.app_menu(app.slug, user)
    assert {
        "current_app": app,
        "current_app_admin": False,
        "show_app_menu": False,
        "show_settings": False,
    } == result


@pytest.mark.django_db
def test_app_menu_anonymous_user(django_assert_num_queries):
    app = AppFactory()
    with django_assert_num_queries(1):
        result = App.objects.app_menu(app.slug, AnonymousUser())
    assert {
        "current_app": app,
        "current_app_admin": False,
        "show_app_menu": False,
        "show_settings": False,
    } == result


@pytest.mark.django_db
def test_app_menu_app_administrator(django_assert_num_queries):
    user = UserFactory()
    app = AppFactory()
    app.administrators.add(user)
    with django_assert_num_queries(4):
        result = App.objects.app_menu(app.slug, user)
    assert {
        "current_app": app,
        "current_app_admin": True,
        "show_app_menu": False,
        "show_settings": True,
    } == result


@pytest.mark.django_db
def test_app_menu_app_superuser(django_assert_num_queries):
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    with django_assert_num_queries(4):
        result = App.objects.app_menu(None, user)
    assert {
        "current_app": None,
        "current_app_admin": False,
        "show_app_menu": False,
        "show_settings": True,
    } == result


@pytest.mark.django_db
def test_app_menu_contact_administrator(django_assert_num_queries):
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.contact_administrators.add(user)
    with django_assert_num_queries(5):
        result = App.objects.app_menu(None, user)
    assert {
        "current_app": None,
        "current_app_admin": False,
        "show_app_menu": False,
        "show_settings": True,
    } == result


@pytest.mark.django_db
def test_app_menu_two_apps(django_assert_num_queries):
    """The user has access to two apps, so show the app menu."""
    user = UserFactory()
    group = GroupFactory()
    user.groups.add(group)
    # app1
    app1 = AppFactory(slug="app1")
    app1.global_group.add(group)
    # app2
    app2 = AppFactory(slug="app2")
    app2.global_group.add(group)
    with django_assert_num_queries(7):
        result = App.objects.app_menu(app1.slug, user)
    assert {
        "current_app": app1,
        "current_app_admin": False,
        "show_app_menu": True,
        "show_settings": False,
    } == result


@pytest.mark.django_db
def test_app_menu_one_app(django_assert_num_queries):
    """The user has access to only one app, so make this the current app."""
    user = UserFactory()
    group = GroupFactory()
    user.groups.add(group)
    app = AppFactory(slug="app1")
    app.global_group.add(group)
    with django_assert_num_queries(7):
        result = App.objects.app_menu(None, user)
    assert {
        "current_app": app,
        "current_app_admin": False,
        "show_app_menu": False,
        "show_settings": False,
    } == result


@pytest.mark.django_db
def test_can_edit_contact():
    app = AppFactory(name="c", url_name_contact_edit="project.dash")
    assert app.can_edit_contact() is True


@pytest.mark.django_db
def test_can_edit_contact_not():
    app = AppFactory(name="c", url_name_contact_edit="")
    assert app.can_edit_contact() is False


@pytest.mark.django_db
def test_create_app():
    app = App.objects.create_app(
        slug="citrus",
        name="Orange",
        menu="Org",
        icon="fa-search",
        hide_app_groups=True,
        url_name_contact_edit="example.contact.edit.crm",
        url_dash="https://www.kbsoftware.co.uk/dash/",
        open_in_new_tab=True,
    )
    assert "citrus" == app.slug
    assert "Orange" == app.name
    assert "Org" == app.menu
    assert "fa-search" == app.icon
    assert app.hide_app_groups is True
    assert "https://www.kbsoftware.co.uk/dash/" == app.url_dash
    assert "example.contact.edit.crm" == app.url_name_contact_edit
    assert app.open_in_new_tab is True


@pytest.mark.parametrize(
    "slug",
    [
        "app_administrator",
        "app_superuser",
        "contact_administrator",
        "show_app_menu",
        "show_settings",
    ],
)
@pytest.mark.django_db
def test_create_app_invalid_slug(slug):
    with pytest.raises(AppError) as e:
        App.objects.create_app(
            slug=slug,
            name="Orange",
            menu="Org",
            icon="fa-search",
            hide_app_groups=False,
            url_name_contact_edit="example.contact.edit.crm",
            url_dash="https://www.kbsoftware.co.uk/dash/",
            open_in_new_tab=False,
        )
    assert (
        "The slug for the app ('{}') cannot contain "
        "('app_administrator', 'app_superuser', 'contact_administrator', "
        "'show_app_menu', 'show_settings')".format(slug)
    ) in str(e.value)


@pytest.mark.django_db
def test_current():
    app = AppFactory(name="b")
    AppFactory(name="c")
    AppFactory(name="a")
    app.set_deleted(UserFactory())
    assert ["a", "c"] == [x.name for x in App.objects.current()]


@pytest.mark.django_db
def test_is_administrator():
    admin = UserFactory(username="admin")
    app = AppFactory(slug=MyApps.CRM)
    app.administrators.add(admin)
    assert App.objects.is_administrator(MyApps.CRM, admin) is True


@pytest.mark.django_db
def test_is_administrator_app_superuser():
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    assert App.objects.is_administrator(MyApps.CRM, user) is True


@pytest.mark.django_db
def test_is_administrator_for_any_app():
    user = UserFactory()
    AppFactory(slug=MyApps.ACCOUNTS)
    app = AppFactory(slug=MyApps.CRM)
    app.administrators.add(user)
    assert App.objects.is_administrator_for_any_app(user) is True


@pytest.mark.django_db
def test_is_administrator_for_any_app_app_superuser():
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    assert App.objects.is_administrator_for_any_app(user) is True


@pytest.mark.django_db
def test_is_administrator_no_app():
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    assert App.objects.is_administrator(None, user) is True


@pytest.mark.django_db
def test_is_administrator_not():
    admin = UserFactory(username="admin")
    app = AppFactory(slug=MyApps.CRM)
    app.administrators.add(admin)
    assert App.objects.is_administrator(MyApps.CRM, UserFactory()) is False


@pytest.mark.django_db
def test_is_administrator_superuser():
    superuser = UserFactory(username="super", is_superuser=True)
    AppFactory(slug=MyApps.CRM)
    assert App.objects.is_administrator(MyApps.CRM, superuser) is True


@pytest.mark.django_db
def test_app_perms():
    user = UserFactory(username="user1")
    group = GroupFactory()
    user.groups.add(group)
    # app1
    app1 = AppFactory(slug="app1")
    app1.administrators.add(user)
    # app2
    app2 = AppFactory(slug="app2")
    app2.global_group.add(group)
    # app3
    AppFactory(slug="app3")
    # app4
    app4 = AppFactory(slug="app4")
    app4_group = AppGroupFactory(app=app4, name="app4-group")
    app4.app_group.add(app4_group)
    AppGroupUserFactory(app_group=app4_group, user=user)
    # test
    result = App.objects.app_perms(user)
    assert {
        "app1": {"access": True, "admin": True},
        "app2": {"access": True, "admin": False},
        "app4": {"access": True, "admin": False},
        "app_administrator": True,
        "app_superuser": False,
        "contact_administrator": False,
        "show_app_menu": True,
        "show_settings": True,
    } == result
    # check defaultdict
    assert result["app5"] == {"access": False, "admin": False}


@pytest.mark.django_db
def test_app_perms_app_superuser():
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    result = App.objects.app_perms(user)
    assert {
        "app_administrator": True,
        "app_superuser": True,
        "contact_administrator": False,
        "show_app_menu": False,
        "show_settings": True,
    } == result
    assert result["myapp"] == {"access": True, "admin": True}


@pytest.mark.django_db
def test_app_perms_user_not_logged_in():
    result = App.objects.app_perms(AnonymousUser())
    assert {
        "app_administrator": False,
        "app_superuser": False,
        "contact_administrator": False,
        "show_app_menu": False,
        "show_settings": False,
    } == result
    assert result["myapp"] == {"access": False, "admin": False}


@pytest.mark.django_db
def test_app_perms_superuser():
    user = UserFactory(is_superuser=True)
    result = App.objects.app_perms(user)
    assert {
        "app_administrator": True,
        "app_superuser": True,
        "contact_administrator": True,
        "show_app_menu": False,
        "show_settings": True,
    } == result
    assert result["myapp"] == {"access": True, "admin": True}


@pytest.mark.django_db
def test_str():
    app = AppFactory(slug="apple")
    assert "apple" == str(app)


@pytest.mark.django_db
def test_user_apps():
    """Which apps can be accessed by this user?"""
    user = UserFactory()
    # apps
    a1 = AppFactory(slug="a1")
    a2 = AppFactory(slug="a2")
    a3 = AppFactory(slug="a3")
    a4 = AppFactory(slug="a4")
    a5 = AppFactory(slug="a5")
    # groups
    g1 = GroupFactory(name="g1")
    g2 = GroupFactory(name="g2")
    g3 = GroupFactory(name="g3")
    # application groups
    g4 = AppGroupFactory(app=a4, name="g4")
    g5 = AppGroupFactory(app=a5, name="g5")
    # global groups
    a1.global_group.add(g1)
    a1.global_group.add(g3)
    a2.global_group.add(g2)
    a3.global_group.add(g3)
    a5.global_group.add(g3)
    # application groups
    a4.app_group.add(g4)
    a5.app_group.add(g5)
    # user groups
    user.groups.add(g1)
    user.groups.add(g3)
    AppGroupUserFactory(app_group=g4, user=user)
    AppGroupUserFactory(app_group=g5, user=user)
    apps = App.objects.user_apps(user)
    assert ["a1", "a3", "a4", "a5"] == [x.slug for x in apps]


@pytest.mark.django_db
def test_user_apps_app_administrator():
    user = UserFactory()
    app = AppFactory(slug="a1")
    app.administrators.add(user)
    apps = App.objects.user_apps(user)
    assert ["a1"] == [x.slug for x in apps]


@pytest.mark.django_db
def test_user_apps_app_superuser():
    AppFactory(slug="a1")
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    apps = App.objects.user_apps(user)
    assert ["a1"] == [x.slug for x in apps]


@pytest.mark.django_db
def test_user_dash():
    """Apps dashboard because we have access to two apps."""
    user = UserFactory()
    g1 = GroupFactory(name="g1")
    a1 = AppFactory(slug="a1")
    a2 = AppFactory(slug="a2")
    a1.global_group.add(g1)
    a2.global_group.add(g1)
    user.groups.add(g1)
    assert reverse("apps.list") == App.objects.user_dash_url(user)


@pytest.mark.django_db
def test_user_dash_one_app():
    """App dashboard because we only have access to one app."""
    user = UserFactory()
    g1 = GroupFactory(name="g1")
    a1 = AppFactory(slug="a1", url_dash="https://www.kbsoftware.co.uk/dash/")
    a1.global_group.add(g1)
    a2 = AppFactory(slug="a2")
    a2.global_group.add(GroupFactory())
    user.groups.add(g1)
    assert "https://www.kbsoftware.co.uk/dash/" == App.objects.user_dash_url(
        user
    )


@pytest.mark.django_db
def test_user_dash_none():
    """Apps dashboard because we don't have access to any apps."""
    user = UserFactory()
    assert reverse("apps.list") == App.objects.user_dash_url(user)
