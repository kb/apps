# -*- encoding: utf-8 -*-
import pytest

from apps.models import AppGroup
from apps.tests.factories import (
    AppFactory,
    AppGroupFactory,
    AppGroupUserFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_app_groups_for_app_user():
    user = UserFactory()
    friends = AppFactory(name="Friends")
    app = AppFactory()
    AppGroupUserFactory(
        app_group=AppGroupFactory(app=app, name="Aa"), user=user
    )
    AppGroupUserFactory(
        app_group=AppGroupFactory(app=friends, name="Bb"), user=user
    )
    AppGroupUserFactory(
        app_group=AppGroupFactory(app=app, name="Cc"), user=user
    )
    app_group = AppGroupFactory(app=app, name="Dd")
    AppGroupUserFactory(app_group=app_group, user=user)
    app_group.set_deleted(UserFactory())
    app_group_user = AppGroupUserFactory(
        app_group=AppGroupFactory(app=app, name="Ee"), user=user
    )
    app_group_user.set_deleted(UserFactory())
    qs = AppGroup.objects.app_groups_for_app_user(app, user)
    assert ["Aa", "Cc"] == [x.name for x in qs]


@pytest.mark.django_db
def test_app_groups_for_app_user_another_user_deleted():
    """Check for issue where another user deleted the app group.

    In the test below, two users are linked to ``app_group_c``.

    - ``user_1`` has NOT deleted the ``AppGroupUser``
    - ``user_2`` has deleted the ``AppGroupUser``.

    Previous versions of the ``app_groups_for_app_user`` method did not
    return the ``AppGroup`` for the user who didn't have the deleted
    record.
    Also see ``test_app_groups_for_user_another_user_deleted``

    """
    user_1 = UserFactory()
    user_2 = UserFactory()
    app = AppFactory(slug="my-app", name="My App")
    app_group = AppGroupFactory(app=app, name="Aa")
    # app_group + user_1
    AppGroupUserFactory(app_group=app_group, user=user_1)
    # app_group + user_2
    x = AppGroupUserFactory(app_group=app_group, user=user_2)
    x.set_deleted(UserFactory())
    qs = AppGroup.objects.app_groups_for_app_user(app, user_1)
    assert ["Aa"] == [x.name for x in qs]


@pytest.mark.django_db
def test_app_groups_for_user():
    user = UserFactory()
    friends = AppFactory(name="Friends")
    app = AppFactory()
    AppGroupUserFactory(
        app_group=AppGroupFactory(app=app, name="Aa"), user=user
    )
    AppGroupUserFactory(
        app_group=AppGroupFactory(app=friends, name="Bb"), user=user
    )
    AppGroupUserFactory(
        app_group=AppGroupFactory(app=app, name="Cc"), user=user
    )
    app_group = AppGroupFactory(app=app, name="Dd")
    AppGroupUserFactory(app_group=app_group, user=user)
    app_group.set_deleted(UserFactory())
    app_group_user = AppGroupUserFactory(
        app_group=AppGroupFactory(app=app, name="Ee"), user=user
    )
    app_group_user.set_deleted(UserFactory())
    qs = AppGroup.objects.app_groups_for_user(user)
    assert ["Aa", "Bb", "Cc"] == [x.name for x in qs.order_by("name")]


@pytest.mark.django_db
def test_app_groups_for_user_another_user_deleted():
    """Check for issue where another user deleted the app group.

    In the test below, two users are linked to ``app_group_c``.

    - ``user_1`` has NOT deleted the ``AppGroupUser``
    - ``user_2`` has deleted the ``AppGroupUser``.

    Previous versions of the ``app_groups_for_user`` method did not
    return the ``AppGroup`` for the user who didn't have the deleted
    record.
    Also see ``test_app_groups_for_app_user_another_user_deleted``

    """
    user_1 = UserFactory()
    user_2 = UserFactory()
    app = AppFactory(slug="my-app", name="My App")
    app_group = AppGroupFactory(app=app, name="Aa")
    # app_group + user_1
    AppGroupUserFactory(app_group=app_group, user=user_1)
    # app_group + user_2
    x = AppGroupUserFactory(app_group=app_group, user=user_2)
    x.set_deleted(UserFactory())
    qs = AppGroup.objects.app_groups_for_user(user_1)
    assert ["Aa"] == [x.name for x in qs]


@pytest.mark.django_db
def test_create_app_group():
    app = AppFactory(name="Fruit")
    app_group = AppGroup.objects.create_app_group(app, "Apple")
    assert "Apple (Fruit)" == str(app_group)
    assert not app_group.is_archived


@pytest.mark.django_db
def test_init_app_group():
    app = AppFactory(name="Fruit")
    app_group = AppGroup.objects.init_app_group(app, "Apple")
    assert "Apple (Fruit)" == str(app_group)


@pytest.mark.django_db
def test_init_app_group_exists():
    app = AppFactory(name="Fruit")
    AppGroupFactory(app=app, name="Apple")
    app_group = AppGroup.objects.init_app_group(app, "Apple")
    assert "Apple (Fruit)" == str(app_group)
    assert 1 == AppGroup.objects.count()


@pytest.mark.django_db
def test_str():
    app_group = AppGroupFactory(app=AppFactory(name="Fruit"), name="Orange")
    assert "Orange (Fruit)" == str(app_group)
