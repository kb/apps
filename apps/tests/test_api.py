# -*- encoding: utf-8 -*-
import pytest

from django.contrib.auth.models import User
from django.urls import reverse
from http import HTTPStatus

from api.tests.fixture import api_client, api_client_auth
from apps.models import AppGroup, AppGroupUser
from apps.tests.factories import (
    AppFactory,
    AppGroupFactory,
    AppGroupUserFactory,
)
from base.url_utils import url_with_querystring
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_api_app_list(api_client_auth):
    user = UserFactory(username="web")
    app_1 = AppFactory(
        slug="apple",
        name="Apple",
        icon="fa-adjust",
        url_dash="https://www.kbsoftware.co.uk/apple/",
        open_in_new_tab=False,
    )
    app_2 = AppFactory(
        slug="lemon",
        name="Lemon",
        icon="fa-cogs",
        url_dash="https://www.kbsoftware.co.uk/lemon/",
        open_in_new_tab=True,
    )
    for app in [app_1, app_2]:
        app_group = AppGroup.objects.init_app_group(
            app, "{} Grocers".format(app.name)
        )
        AppGroupUser.objects.init_app_group_user(app_group, user)
        app.app_group.add(app_group)
    response = api_client_auth(user).get(
        reverse("api:app-list"), content_type="application/json"
    )
    assert HTTPStatus.OK == response.status_code, response.data
    data = response.json()
    assert {
        "links": {
            "first": "http://testserver/api/0.1/apps/?page%5Bnumber%5D=1",
            "last": "http://testserver/api/0.1/apps/?page%5Bnumber%5D=1",
            "next": None,
            "prev": None,
        },
        "data": [
            {
                "type": "App",
                "id": f"{app_1.pk}",
                "attributes": {
                    "slug": "apple",
                    "name": "Apple",
                    "menu": "",
                    "icon": "fa-adjust",
                    "url-dash": "https://www.kbsoftware.co.uk/apple/",
                    "open-in-new-tab": False,
                },
            },
            {
                "type": "App",
                "id": f"{app_2.pk}",
                "attributes": {
                    "slug": "lemon",
                    "name": "Lemon",
                    "menu": "",
                    "icon": "fa-cogs",
                    "url-dash": "https://www.kbsoftware.co.uk/lemon/",
                    "open-in-new-tab": True,
                },
            },
        ],
        "meta": {"pagination": {"page": 1, "pages": 1, "count": 2}},
    } == data


@pytest.mark.django_db
def test_api_app_group_list(api_client_auth):
    user = UserFactory()
    app_1 = AppFactory()
    app_group_1 = AppGroupFactory(app=app_1, name="AppGroup1")
    app_2 = AppFactory()
    app_group_2 = AppGroupFactory(app=app_2, name="AppGroup2")
    response = api_client_auth(user).get(
        reverse("api:appgroup-list"), content_type="application/json"
    )
    assert HTTPStatus.OK == response.status_code, response.data
    data = response.json()
    assert "data" in data
    assert 2 == len(data["data"])
    assert {
        "links": {
            "first": "http://testserver/api/0.1/appgroups/?page%5Bnumber%5D=1",
            "last": "http://testserver/api/0.1/appgroups/?page%5Bnumber%5D=1",
            "next": None,
            "prev": None,
        },
        "data": [
            {
                "type": "AppGroup",
                "id": f"{app_group_1.pk}",
                "attributes": {"name": "AppGroup1"},
                "relationships": {
                    "app": {"data": {"type": "App", "id": f"{app_1.pk}"}}
                },
            },
            {
                "type": "AppGroup",
                "id": f"{app_group_2.pk}",
                "attributes": {"name": "AppGroup2"},
                "relationships": {
                    "app": {"data": {"type": "App", "id": f"{app_2.pk}"}}
                },
            },
        ],
        "meta": {"pagination": {"page": 1, "pages": 1, "count": 2}},
    } == data


@pytest.mark.django_db
def test_api_app_group_list_slug(api_client_auth):
    user = UserFactory()
    app_1 = AppFactory(slug="crm")
    AppGroupFactory(app=app_1, name="AppGroup1")
    app_2 = AppFactory(slug="workflow")
    app_group_2 = AppGroupFactory(app=app_2, name="AppGroup2")
    parameters = {"filter[app__slug]": "workflow"}
    url = url_with_querystring(reverse("api:appgroup-list"), **parameters)
    response = api_client_auth(user).get(
        url, content_type="application/vnd.api+json"
    )
    assert HTTPStatus.OK == response.status_code, response.data
    data = response.json()
    assert "data" in data
    assert 1 == len(data["data"])
    assert {
        "links": {
            "first": "http://testserver/api/0.1/appgroups/?filter%5Bapp__slug%5D=workflow&page%5Bnumber%5D=1",
            "last": "http://testserver/api/0.1/appgroups/?filter%5Bapp__slug%5D=workflow&page%5Bnumber%5D=1",
            "next": None,
            "prev": None,
        },
        "data": [
            {
                "type": "AppGroup",
                "id": f"{app_group_2.pk}",
                "attributes": {"name": "AppGroup2"},
                "relationships": {
                    "app": {"data": {"type": "App", "id": f"{app_2.pk}"}}
                },
            }
        ],
        "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
    } == data


@pytest.mark.django_db
def test_api_app_group_user_list(api_client_auth):
    user = UserFactory()
    app = AppFactory(slug="workflow")
    app_group_1 = AppGroupFactory(app=app, name="AppGroup1")
    app_group_user_1 = AppGroupUserFactory(
        app_group=app_group_1,
        user=UserFactory(first_name="P", last_name="Kimber"),
    )
    # app_2 = AppFactory(slug="crm")
    app_group_2 = AppGroupFactory(app=app, name="AppGroup2")
    AppGroupUserFactory(app_group=app_group_2)
    # app_3 = AppFactory(slug="workflow")
    # app_group_3 = AppGroupFactory(app=app_3, name="AppGroup3")
    app_group_user_3 = AppGroupUserFactory(
        app_group=app_group_1,
        user=UserFactory(first_name="A", last_name="Apple"),
    )
    # test
    parameters = {"filter[app_group__id]": app_group_1.pk}
    url = url_with_querystring(reverse("api:appgroupuser-list"), **parameters)
    # url = url_with_querystring(
    #    reverse("api:appgroupuser-list"), group_id=app_group_1.pk
    # )
    response = api_client_auth(user).get(
        url, content_type="application/vnd.api+json"
    )
    assert HTTPStatus.OK == response.status_code, response.data
    data = response.json()
    assert {
        "links": {
            "first": f"http://testserver/api/0.1/appgroupusers/?filter%5Bapp_group__id%5D={app_group_1.pk}&page%5Bnumber%5D=1",
            "last": f"http://testserver/api/0.1/appgroupusers/?filter%5Bapp_group__id%5D={app_group_1.pk}&page%5Bnumber%5D=1",
            "next": None,
            "prev": None,
        },
        "data": [
            {
                "type": "AppGroupUser",
                "id": f"{app_group_user_1.pk}",
                "attributes": {"user-full-name": "P Kimber"},
                "relationships": {
                    "app-group": {
                        "data": {"type": "AppGroup", "id": f"{app_group_1.pk}"}
                    }
                },
            },
            {
                "type": "AppGroupUser",
                "id": f"{app_group_user_3.pk}",
                "attributes": {"user-full-name": "A Apple"},
                "relationships": {
                    "app-group": {
                        "data": {"type": "AppGroup", "id": f"{app_group_1.pk}"}
                    }
                },
            },
        ],
        "meta": {"pagination": {"page": 1, "pages": 1, "count": 2}},
    } == data
