# -*- encoding: utf-8 -*-
import pytest

from apps.models import AppGroupUser
from apps.tests.factories import (
    AppFactory,
    AppGroupFactory,
    AppGroupUserFactory,
)
from contact.tests.factories import ContactFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_current(django_assert_num_queries):
    """List of users for an application group.

    .. note:: Will exclude deleted contacts.

    """
    app_1 = AppFactory()
    app_2 = AppFactory()
    group_1 = AppGroupFactory(app=app_1, name="g1")
    group_2 = AppGroupFactory(app=app_2, name="g2")
    user_1 = UserFactory(username="u1")
    user_2 = UserFactory(username="u2")
    user_3 = UserFactory(username="u3")
    user_4 = UserFactory(username="u4")
    user_5 = UserFactory(username="u5")
    contact_1 = ContactFactory(user=user_1)
    contact_2 = ContactFactory(user=user_2)
    contact_3 = ContactFactory(user=user_3)
    contact_4 = ContactFactory(user=user_4)
    contact_5 = ContactFactory(user=user_5)
    AppGroupUserFactory(app_group=group_1, user=user_1)
    AppGroupUserFactory(app_group=group_2, user=user_2)
    app_group_user = AppGroupUserFactory(app_group=group_1, user=user_3)
    app_group_user.set_deleted(user_3)
    contact_4.set_deleted(user_4)
    AppGroupUserFactory(app_group=group_1, user=user_4)
    AppGroupUserFactory(app_group=group_1, user=user_5)
    with django_assert_num_queries(1):
        app_group_users = [x for x in AppGroupUser.objects.current()]
    assert set(["u1", "u2", "u5"]) == set(
        [x.user.username for x in app_group_users]
    )


@pytest.mark.django_db
def test_init_app_group_user():
    app_group = AppGroupFactory()
    user = UserFactory()
    x = AppGroupUser.objects.init_app_group_user(app_group, user)
    assert app_group == x.app_group
    assert user == x.user


@pytest.mark.django_db
def test_init_app_group_user_is_deleted():
    app_group = AppGroupFactory()
    user = UserFactory()
    x = AppGroupUser.objects.init_app_group_user(app_group, user)
    AppGroupUser.objects.remove_user_from_app_group(
        user, app_group, UserFactory()
    )
    x.refresh_from_db()
    assert x.is_deleted is True
    x = AppGroupUser.objects.init_app_group_user(app_group, user)
    x.refresh_from_db()
    assert x.is_deleted is False


@pytest.mark.django_db
def test_remove_user_from_app_group():
    app_group = AppGroupFactory()
    user = UserFactory()
    x = AppGroupUser.objects.init_app_group_user(app_group, user)
    assert x.is_deleted is False
    AppGroupUser.objects.remove_user_from_app_group(
        user, app_group, UserFactory()
    )
    x.refresh_from_db()
    assert x.is_deleted is True


@pytest.mark.django_db
def test_remove_user_from_app_group_does_not_exist():
    """If the app group is not linked to the user, then ignore."""
    app_group = AppGroupFactory()
    user = UserFactory()
    AppGroupUser.objects.remove_user_from_app_group(
        user, app_group, UserFactory()
    )
    with pytest.raises(AppGroupUser.DoesNotExist):
        AppGroupUser.objects.get(user=user, app_group=app_group)


@pytest.mark.django_db
def test_str():
    user = UserFactory(username="pat")
    app = AppFactory()
    app_group_user = AppGroupUserFactory(
        app_group=AppGroupFactory(app=app, name="Apple"), user=user
    )
    assert "Apple: pat" == str(app_group_user)


@pytest.mark.django_db
def test_update_app_group_user():
    user = UserFactory()
    ContactFactory(user=user)
    app_1 = AppFactory()
    app_2 = AppFactory()
    app_group_1 = AppGroupFactory(app=app_1, name="g1")
    app_group_2 = AppGroupFactory(app=app_1, name="g2")
    app_group_3 = AppGroupFactory(app=app_1, name="g3")
    app_group_4 = AppGroupFactory(app=app_2, name="g4")
    AppGroupUserFactory(app_group=app_group_1, user=user)
    AppGroupUserFactory(app_group=app_group_3, user=user)
    AppGroupUserFactory(app_group=app_group_4, user=user)
    # check
    assert ["g1", "g3"] == [
        x.app_group.name for x in AppGroupUser.objects.user_groups(app_1, user)
    ]
    assert ["g4"] == [
        x.app_group.name for x in AppGroupUser.objects.user_groups(app_2, user)
    ]
    # test
    AppGroupUser.objects.update_app_group_user(
        app_1, [app_group_1, app_group_2], user
    )
    # check
    assert ["g1", "g2"] == [
        x.app_group.name for x in AppGroupUser.objects.user_groups(app_1, user)
    ]
    assert ["g4"] == [
        x.app_group.name for x in AppGroupUser.objects.user_groups(app_2, user)
    ]


@pytest.mark.django_db
def test_users(django_assert_num_queries):
    """List of users for an application group.

    .. note:: Will exclude deleted contacts.

    """
    app_1 = AppFactory()
    app_2 = AppFactory()
    group_1 = AppGroupFactory(app=app_1, name="g1")
    group_2 = AppGroupFactory(app=app_2, name="g2")
    user_1 = UserFactory(username="u1")
    user_2 = UserFactory(username="u2")
    user_3 = UserFactory(username="u3")
    user_4 = UserFactory(username="u4")
    user_5 = UserFactory(username="u5")
    contact_1 = ContactFactory(user=user_1)
    contact_2 = ContactFactory(user=user_2)
    contact_3 = ContactFactory(user=user_3)
    contact_4 = ContactFactory(user=user_4)
    contact_5 = ContactFactory(user=user_5)
    AppGroupUserFactory(app_group=group_1, user=user_1)
    AppGroupUserFactory(app_group=group_2, user=user_2)
    app_group_user = AppGroupUserFactory(app_group=group_1, user=user_3)
    app_group_user.set_deleted(user_3)
    contact_4.set_deleted(user_4)
    AppGroupUserFactory(app_group=group_1, user=user_4)
    AppGroupUserFactory(app_group=group_1, user=user_5)
    with django_assert_num_queries(3):
        result = [x.user.username for x in AppGroupUser.objects.users(group_1)]
    assert ["u1", "u5"] == result


@pytest.mark.django_db
def test_users_in_groups():
    app_1 = AppFactory()
    app_2 = AppFactory()
    group_1 = AppGroupFactory(app=app_1, name="g1")
    group_2 = AppGroupFactory(app=app_2, name="g2")
    user_1 = UserFactory(username="u1")
    ContactFactory(user=user_1)
    user_2 = UserFactory(username="u2")
    ContactFactory(user=user_2)
    user_3 = UserFactory(username="u3")
    ContactFactory(user=user_3)
    user_4 = UserFactory(username="u4")
    contact_4 = ContactFactory(user=user_4)
    contact_4.set_deleted(user_4)
    user_5 = UserFactory(username="u5")
    ContactFactory(user=user_5)
    AppGroupUserFactory(app_group=group_1, user=user_1)
    AppGroupUserFactory(app_group=group_2, user=user_2)
    app_group_user = AppGroupUserFactory(app_group=group_1, user=user_3)
    app_group_user.set_deleted(UserFactory())
    AppGroupUserFactory(app_group=group_1, user=user_4)
    AppGroupUserFactory(app_group=group_1, user=user_5)
    assert ["u1", "u5"] == [
        x.user.username for x in AppGroupUser.objects.users_in_groups([group_1])
    ]
