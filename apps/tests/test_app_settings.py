# -*- encoding: utf-8 -*-
import pytest

from apps.models import AppSettings
from apps.tests.factories import AppSettingsFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_is_contact_administrator():
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    app_settings.contact_administrators.add(user)
    assert AppSettings.objects.is_contact_administrator(user) is True


@pytest.mark.django_db
def test_is_contact_administrator_superuser():
    user = UserFactory(is_superuser=True)
    assert AppSettings.objects.is_contact_administrator(user) is True


@pytest.mark.django_db
def test_is_contact_administrator_not():
    user = UserFactory()
    assert AppSettings.objects.is_contact_administrator(user) is False


@pytest.mark.django_db
def test_is_app_superuser():
    user = UserFactory(is_superuser=True)
    assert AppSettings.objects.is_app_superuser(user) is True


@pytest.mark.django_db
def test_is_app_superuser_application():
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    assert AppSettings.objects.is_app_superuser(user) is True


@pytest.mark.django_db
def test_is_app_superuser_not():
    user = UserFactory()
    assert AppSettings.objects.is_app_superuser(user) is False


@pytest.mark.django_db
def test_load():
    app_settings = AppSettingsFactory()
    assert app_settings == AppSettings.load()


@pytest.mark.django_db
def test_load_does_not_exist():
    app_settings = AppSettings.load()
    with pytest.raises(ValueError) as e:
        app_settings.app_superusers.all()
    assert (
        'needs to have a value for field "id" before '
        "this many-to-many relationship can be used"
    ) in str(e.value)


@pytest.mark.django_db
def test_str():
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(UserFactory(username="pat"))
    assert ("App superusers ['pat'] " "Contact administrators []") == str(
        app_settings
    )


@pytest.mark.django_db
def test_str_load():
    app_settings = AppSettings.load()
    assert "App superusers [] Contact administrators []" == str(app_settings)
