# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from apps.tests.factories import AppFactory, AppGroupFactory, AppSettingsFactory
from login.tests.factories import GroupFactory, TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check


def _contact_administrator():
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.contact_administrators.add(user)
    return user


@pytest.mark.django_db
def test_app_group_create(perm_check):
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    AppGroupFactory(app=app)
    url = reverse("apps.group.create", args=[app.slug])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_app_group_create_administrator(client):
    user = UserFactory()
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    app.administrators.add(user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("apps.group.create", args=[app.slug])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_app_group_delete(perm_check):
    app_group = AppGroupFactory(
        app=AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    )
    url = reverse("apps.group.delete", args=[app_group.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_app_group_delete_administrator(client):
    user = UserFactory()
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    app.administrators.add(user)
    app_group = AppGroupFactory(app=app)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("apps.group.delete", args=[app_group.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_app_group_detail(perm_check):
    app_group = AppGroupFactory()
    url = reverse("apps.group.detail", args=[app_group.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_app_group_detail_administrator(client):
    user = UserFactory()
    app = AppFactory()
    app.administrators.add(user)
    app_group = AppGroupFactory(app=app)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("apps.group.detail", args=[app_group.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_app_group_list(perm_check):
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    AppGroupFactory(app=app)
    url = reverse("apps.group.list", args=[app.slug])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_app_group_list_administrator(client):
    user = UserFactory()
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    app.administrators.add(user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("apps.group.list", args=[app.slug])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_app_group_update(perm_check):
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    app_group = AppGroupFactory(app=app)
    url = reverse("apps.group.update", args=[app_group.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_app_group_update_administrator(client):
    user = UserFactory()
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    app.administrators.add(user)
    app_group = AppGroupFactory(app=app)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("apps.group.update", args=[app_group.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_app_list(perm_check):
    url = reverse("apps.list")
    perm_check.auth(url)


@pytest.mark.django_db
def test_app_settings(perm_check):
    url = reverse("apps.settings.update")
    perm_check.superuser(url)


@pytest.mark.django_db
def test_app_update_admin(perm_check):
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    url = reverse("apps.update.administrator", args=[app.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_app_update_admin_app_administrator(client):
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(UserFactory())
    user = UserFactory()
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    app.administrators.add(user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("apps.update.administrator", args=[app.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_app_update_super(perm_check):
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    url = reverse("apps.update.superuser", args=[app.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_app_update_super_app_superuser(client):
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    app = AppFactory(url_dash="https://www.kbsoftware.co.uk/dash/")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("apps.update.superuser", args=[app.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_group_create(perm_check):
    url = reverse("group.create")
    perm_check.superuser(url)


@pytest.mark.django_db
def test_group_create_contact_administrator(client):
    user = _contact_administrator()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("group.create"))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_group_detail_(perm_check):
    group = GroupFactory()
    url = reverse("group.detail", args=[group.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_group_detail_contact_administrator(client):
    group = GroupFactory()
    user = _contact_administrator()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("group.detail", args=[group.pk]))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_group_list(perm_check):
    GroupFactory()
    url = reverse("group.list")
    perm_check.superuser(url)


@pytest.mark.django_db
def test_group_list_contact_administrator(client):
    GroupFactory()
    user = _contact_administrator()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("group.list"))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_group_update(perm_check):
    group = GroupFactory()
    url = reverse("group.update", args=[group.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_group_update_contact_administrator(client):
    group = GroupFactory()
    user = _contact_administrator()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("group.update", args=[group.pk]))
    assert HTTPStatus.OK == response.status_code
