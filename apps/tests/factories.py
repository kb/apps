# -*- encoding: utf-8 -*-
import factory

from apps.models import App, AppGroup, AppGroupUser, AppSettings
from login.tests.factories import UserFactory


class AppFactory(factory.django.DjangoModelFactory):
    icon = "fa-adjust"
    url_name_contact_edit = "url.name.for.contact.edit.set.in.app.factory"
    url_dash = "https://www.kbsoftware.co.uk/apps/url/for/dash/"

    class Meta:
        model = App

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)


class AppGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AppGroup

    app = factory.SubFactory(AppFactory)

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)


class AppGroupUserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AppGroupUser

    app_group = factory.SubFactory(AppGroupFactory)
    user = factory.SubFactory(UserFactory)


class AppSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AppSettings
