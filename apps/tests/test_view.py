# -*- encoding: utf-8 -*-
import pytest

from django.contrib.auth.models import Group
from django.core.management import call_command
from django.urls import reverse
from http import HTTPStatus

from apps.models import App, AppGroup, AppSettings
from apps.tests.factories import (
    AppFactory,
    AppGroupFactory,
    AppGroupUserFactory,
    AppSettingsFactory,
)
from contact.tests.factories import ContactFactory
from login.tests.factories import GroupFactory, TEST_PASSWORD, UserFactory


def _contact_administrator():
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.contact_administrators.add(user)
    return user


@pytest.mark.django_db
def test_app_group_create(client):
    app = AppFactory(slug="flower")
    user = UserFactory()
    app.administrators.add(user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {"name": "Primrose"}
    url = reverse("apps.group.create", args=[app.slug])
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("apps.group.list", args=[app.slug])
    assert expect == response.url
    AppGroup.objects.get(app__slug="flower", name="Primrose")


@pytest.mark.django_db
def test_app_group_delete(client):
    app = AppFactory(slug="flower")
    user = UserFactory()
    app.administrators.add(user)
    app_group = AppGroup.objects.init_app_group(app, "Buttercup")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("apps.group.delete", args=[app_group.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("apps.group.list", args=[app.slug])
    assert expect == response.url
    app_group = AppGroup.objects.get(app__slug="flower", name="Buttercup")
    assert app_group.is_deleted is True


@pytest.mark.django_db
def test_app_group_detail(client, django_assert_num_queries):
    app = AppFactory(slug="flower")
    user = UserFactory()
    app.administrators.add(user)
    app_group = AppGroup.objects.init_app_group(app, "Buttercup")
    user_1 = UserFactory(username="u1")
    user_2 = UserFactory(username="u2")
    user_3 = UserFactory(username="u3")
    contact_1 = ContactFactory(user=user_1)
    contact_2 = ContactFactory(user=user_2)
    contact_3 = ContactFactory(user=user_3)
    contact_2.set_deleted(user_2)
    AppGroupUserFactory(app_group=app_group, user=user_1)
    AppGroupUserFactory(app_group=app_group, user=user_2)
    AppGroupUserFactory(app_group=app_group, user=user_3)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("apps.group.detail", args=[app_group.pk])
    with django_assert_num_queries(12):
        response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "app_group_users" in response.context
    assert ["u1", "u3"] == [
        x.user.username for x in response.context["app_group_users"]
    ]


@pytest.mark.django_db
def test_app_group_update(client):
    app = AppFactory(slug="flower")
    user = UserFactory()
    app.administrators.add(user)
    app_group = AppGroup.objects.init_app_group(app, "Buttercup")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("apps.group.update", args=[app_group.pk])
    data = {"name": "Daffodil"}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("apps.group.list", args=[app.slug])
    assert expect == response.url
    with pytest.raises(AppGroup.DoesNotExist):
        AppGroup.objects.get(app__slug="flower", name="Buttercup")
    app_group = AppGroup.objects.get(app__slug="flower", name="Daffodil")
    assert 1 == AppGroup.objects.count()


@pytest.mark.django_db
def test_app_list(client):
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("apps.list"))
    assert HTTPStatus.OK == response.status_code
    assert "app_menu" in response.context
    app_menu = response.context["app_menu"]
    assert "current_app" in app_menu
    assert app_menu["current_app"] is None


@pytest.mark.django_db
def test_app_settings_update(client):
    user = UserFactory(is_staff=True, is_superuser=True)
    ContactFactory(user=user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {"app_superusers": [user.pk], "contact_administrators": [user.pk]}
    response = client.post(reverse("apps.settings.update"), data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("project.settings")
    assert expect == response["Location"]
    assert AppSettings.objects.user_is_app_superuser(user) is True
    assert AppSettings.objects.user_is_contact_administrator(user) is True


@pytest.mark.django_db
def test_app_settings_update_get(client):
    user = UserFactory(username="u1", is_staff=True, is_superuser=True)
    ContactFactory(user=user)
    user_2 = UserFactory(username="u2", is_staff=True, is_superuser=True)
    contact_2 = ContactFactory(user=user_2)
    contact_2.set_deleted(user_2)
    user_3 = UserFactory(username="u3", is_staff=True, is_superuser=True)
    ContactFactory(user=user_3)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {"app_superusers": [user.pk], "contact_administrators": [user.pk]}
    response = client.get(reverse("apps.settings.update"))
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    app_superusers = form.fields["app_superusers"]
    assert ["u1", "u3"] == [x.username for x in app_superusers.queryset]
    contact_administrators = form.fields["contact_administrators"]
    assert ["u1", "u3"] == [x.username for x in contact_administrators.queryset]


@pytest.mark.django_db
def test_app_update_admin(client):
    group = GroupFactory(name="Accounts")
    user = UserFactory(username="tim", is_staff=True, is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    app = AppFactory(
        slug="apple",
        name="Apple",
        icon="fa-adjust",
        url_dash="https://www.kbsoftware.co.uk/dash/",
    )
    app_group = AppGroupFactory(app=app, name="Sales")
    url = reverse("apps.update.administrator", args=[app.pk])
    data = {"app_group": [app_group.pk], "global_group": [group.pk]}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("project.settings")
    assert expect == response["Location"]
    app = App.objects.get(slug="apple")
    assert ["Sales"] == [x.name for x in app.app_group.all()]
    assert ["Accounts"] == [x.name for x in app.global_group.all()]


@pytest.mark.django_db
def test_app_update_admin_get(client):
    GroupFactory(name="Accounts")
    user = UserFactory(username="tim", is_staff=True, is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    app = AppFactory(
        slug="apple",
        name="Apple",
        icon="fa-adjust",
        url_dash="https://www.kbsoftware.co.uk/dash/",
    )
    AppGroupFactory(app=app, name="Sales")
    url = reverse("apps.update.administrator", args=[app.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert ["app_group", "global_group"] == list(form.fields.keys())


@pytest.mark.django_db
def test_app_update_admin_hide_app_groups(client):
    """App administrator to update the fields for the app.

    .. note:: ``hide_app_groups`` is ``True``, so the ``app_group`` data in the
              form post should be ignored.

    """
    group = GroupFactory(name="Accounts")
    user = UserFactory(username="tim", is_staff=True, is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    app = AppFactory(
        slug="apple",
        name="Apple",
        icon="fa-adjust",
        hide_app_groups=True,
        url_dash="https://www.kbsoftware.co.uk/dash/",
    )
    app_group = AppGroupFactory(app=app, name="Sales")
    url = reverse("apps.update.administrator", args=[app.pk])
    data = {"app_group": [app_group.pk], "global_group": [group.pk]}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("project.settings")
    assert expect == response["Location"]
    app = App.objects.get(slug="apple")
    assert ["Accounts"] == [x.name for x in app.global_group.all()]
    assert [] == [x.name for x in app.app_group.all()]


@pytest.mark.django_db
def test_app_update_admin_hide_app_groups_get(client):
    GroupFactory(name="Accounts")
    user = UserFactory(username="tim", is_staff=True, is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    app = AppFactory(
        slug="apple",
        name="Apple",
        icon="fa-adjust",
        hide_app_groups=True,
        url_dash="https://www.kbsoftware.co.uk/dash/",
    )
    AppGroupFactory(app=app, name="Sales")
    url = reverse("apps.update.administrator", args=[app.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert ["global_group"] == list(form.fields.keys())


@pytest.mark.django_db
def test_app_update_superuser(client):
    user = UserFactory(username="tim", is_staff=True, is_superuser=True)
    ContactFactory(user=user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    app = AppFactory(
        slug="apple",
        name="Apple",
        icon="fa-adjust",
        url_dash="https://www.kbsoftware.co.uk/dash/",
    )
    url = reverse("apps.update.superuser", args=[app.pk])
    data = {
        "name": "Orange",
        "menu": "Org",
        "icon": "fa-mobile",
        "administrators": [user.pk],
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("project.settings")
    assert expect == response["Location"]
    app = App.objects.get(slug="apple")
    assert "Orange" == app.name
    assert "Org" == app.menu
    assert "fa-mobile" == app.icon
    assert ["tim"] == [x.username for x in app.administrators.all()]


@pytest.mark.django_db
def test_app_update_superuser_get(client):
    user = UserFactory(username="u1", is_staff=True, is_superuser=True)
    ContactFactory(user=user)
    user_2 = UserFactory(username="u2", is_staff=True, is_superuser=True)
    contact_2 = ContactFactory(user=user_2)
    contact_2.set_deleted(user_2)
    user_3 = UserFactory(username="u3", is_staff=True, is_superuser=True)
    ContactFactory(user=user_3)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    app = AppFactory(
        slug="apple",
        name="Apple",
        icon="fa-adjust",
        url_dash="https://www.kbsoftware.co.uk/dash/",
    )
    url = reverse("apps.update.superuser", args=[app.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    administrators = form.fields["administrators"]
    assert ["u1", "u3"] == [x.username for x in administrators.queryset]


@pytest.mark.django_db
def test_app_update_superuser_no_administrators(client):
    user = UserFactory(username="tim", is_staff=True, is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    app = AppFactory(
        slug="apple",
        name="Apple",
        icon="fa-adjust",
        url_dash="https://www.kbsoftware.co.uk/dash/",
    )
    url = reverse("apps.update.superuser", args=[app.pk])
    data = {"name": "Orange", "menu": "Org", "icon": "fa-mobile"}
    response = client.post(url, data)
    assert HTTPStatus.OK == response.status_code
    assert {"administrators": ["This field is required."]} == response.context[
        "form"
    ].errors


@pytest.mark.django_db
def test_group_create(client):
    user = _contact_administrator()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {"name": "Primrose"}
    url = reverse("group.create")
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("group.list")
    assert expect == response.url
    Group.objects.get(name="Primrose")


@pytest.mark.django_db
def test_group_detail(client):
    """Group detail.

    The group detail view is actually a ``ListView`` because the potential
    number of users in a group is very large.

    """
    user = _contact_administrator()
    group = GroupFactory(name="Primrose")
    group.user_set.add(UserFactory(username="u1"))
    group.user_set.add(UserFactory(username="u2"))
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("group.detail", args=[group.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "group" in response.context
    assert group == response.context["group"]
    assert "user_list" in response.context
    assert ["u1", "u2"] == [x.username for x in response.context["user_list"]]


@pytest.mark.django_db
def test_group_update(client):
    user = _contact_administrator()
    group = GroupFactory(name="Primrose")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {"name": "Daffodil"}
    url = reverse("group.update", args=[group.pk])
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("group.detail", args=[group.pk])
    assert expect == response.url
    with pytest.raises(Group.DoesNotExist):
        Group.objects.get(name="Primrose")
    Group.objects.get(name="Daffodil")
    assert 1 == Group.objects.count()


@pytest.mark.django_db
def test_settings(client):
    call_command("demo_data_apps")
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("project.settings"))
    assert HTTPStatus.OK == response.status_code
    assert "admin_apps" in response.context
    assert "is_contact_administrator" in response.context
    assert "superuser_apps" in response.context
    admin_apps = response.context["admin_apps"]
    assert ["accounts", "crm"] == [x.slug for x in admin_apps]
    superuser_apps = response.context["superuser_apps"]
    assert ["accounts", "crm"] == [x.slug for x in superuser_apps]
    assert response.context["is_contact_administrator"] is True
