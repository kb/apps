# -*- encoding: utf-8 -*-
from django_filters import rest_framework as filters

from apps.models import AppGroup, AppGroupUser


class AppGroupFilter(filters.FilterSet):
    class Meta:
        model = AppGroup
        fields = ["app__slug"]


class AppGroupUserFilter(filters.FilterSet):
    class Meta:
        model = AppGroupUser
        fields = ["app_group__id"]
