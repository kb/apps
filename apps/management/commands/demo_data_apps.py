# -*- encoding: utf-8 -*-
import urllib.parse

from django.conf import settings
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from django.urls import reverse

from apps.models import App, AppGroup, AppGroupUser
from example_apps.models import MyApps
from login.tests.model_maker import make_user


class Command(BaseCommand):
    help = "Demo data for 'apps'"

    def _app_firefox(self, user_firefox):
        app = App.objects.create_app(
            slug=MyApps.ACCOUNTS,
            name="Accounts",
            menu="Accounts",
            icon="fa-firefox",
            hide_app_groups=False,
            url_dash=urllib.parse.urljoin(
                settings.HOST_NAME, reverse("example.dash.accounts")
            ),
            url_name_contact_edit="example.contact.edit.accounts",
            open_in_new_tab=True,
        )
        app_group = AppGroup.objects.create_app_group(app, "Thunderbird")
        app.app_group.add(app_group)
        AppGroupUser.objects.init_app_group_user(
            app_group, make_user("apple.user")
        )
        self.stdout.write("app: {}".format(app.slug))

    def _app_linux(self, user_linux):
        app = App.objects.create_app(
            slug=MyApps.CRM,
            name="CRM",
            menu="CRM",
            icon="fa-linux",
            hide_app_groups=False,
            url_dash=urllib.parse.urljoin(
                settings.HOST_NAME, reverse("example.dash.crm")
            ),
            url_name_contact_edit="example.contact.edit.crm",
            open_in_new_tab=True,
        )
        AppGroup.objects.create_app_group(app, "Budgie")
        AppGroup.objects.create_app_group(app, "Mate")
        app_group = AppGroup.objects.create_app_group(app, "Peppermint")
        app.app_group.add(app_group)
        AppGroupUser.objects.init_app_group_user(app_group, user_linux)
        self.stdout.write("app: {}".format(app.slug))

    def _groups(self):
        Group(name="Developers").save()
        Group(name="Support Team").save()

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        # firefox
        user_firefox = make_user("firefox.user")
        self._app_firefox(user_firefox)
        # linux
        user_linux = make_user("linux.user")
        self._app_linux(user_linux)
        # django groups
        self._groups()
        self.stdout.write("{} - Complete".format(self.help))
