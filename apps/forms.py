# -*- encoding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from apps.models import App, AppGroup, AppSettings, current_users


class IssueModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return "{}/{}".format(obj.document.document_code, obj.issue)


class UserModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return "{} ({})".format(obj.get_full_name(), obj.username)


class AppAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        app = kwargs.pop("app")
        super().__init__(*args, **kwargs)
        data = [("global_group", Group.objects.all().order_by("name"), None)]
        if not app.hide_app_groups:
            qs = AppGroup.objects.app_groups(app).order_by("name")
            data.append(("app_group", qs, "{} group".format(app.menu)))
        for name, qs, label in data:
            f = self.fields[name]
            f.queryset = qs
            if label:
                f.label = label
            f.widget.attrs.update({"class": "chosen-select pure-input-2-3"})
        if app.hide_app_groups:
            del self.fields["app_group"]

    class Meta:
        model = App
        fields = ("app_group", "global_group")


class AppGroupEmptyForm(forms.ModelForm):
    class Meta:
        model = AppGroup
        fields = ()


class AppGroupForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["name"]
        f.widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = AppGroup
        fields = ("name",)


class AppSettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        qs = current_users().order_by("username")
        for name in ("app_superusers", "contact_administrators"):
            f = self.fields[name]
            f.queryset = qs
            f.widget.attrs.update({"class": "chosen-select pure-input-2-3"})

    class Meta:
        model = AppSettings
        fields = ("app_superusers", "contact_administrators")
        field_classes = {
            "app_superusers": UserModelMultipleChoiceField,
            "contact_administrators": UserModelMultipleChoiceField,
        }


class AppSuperForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        user_qs = current_users().order_by("username")
        data = [("administrators", user_qs)]
        for name, qs in data:
            f = self.fields[name]
            f.queryset = qs
            f.widget.attrs.update({"class": "chosen-select pure-input-2-3"})

    class Meta:
        model = App
        fields = ("name", "icon", "menu", "administrators")
        field_classes = {"administrators": UserModelMultipleChoiceField}


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ("name",)
