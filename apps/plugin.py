# -*- encoding: utf-8 -*-
from .models import App


class AppBaseMixin:
    def get_context_data(self_for_instance):
        """Information needed to draw the menu (including the current app).

        For docs, see:
        https://www.kbsoftware.co.uk/docs/app-apps.html#views

        Ticket:
        https://www.kbsoftware.co.uk/crm/ticket/4777/

        """
        app_menu = App.objects.app_menu(
            self_for_instance.current_app_slug, self_for_instance.request.user
        )
        return dict(app_menu=app_menu)
