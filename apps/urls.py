# -*- encoding: utf-8 -*-
from django.urls import re_path

from apps.views import (
    AppAdminUpdateView,
    AppGroupCreateView,
    AppGroupDeleteView,
    AppGroupDetailView,
    AppGroupListView,
    AppGroupUpdateView,
    AppListView,
    AppSettingsUpdateView,
    AppSuperUpdateView,
    GroupCreateView,
    GroupDetailListView,
    GroupListView,
    GroupUpdateView,
)


urlpatterns = [
    re_path(
        r"^settings/update/$",
        view=AppSettingsUpdateView.as_view(),
        name="apps.settings.update",
    ),
    re_path(r"^$", view=AppListView.as_view(), name="apps.list"),
    re_path(
        r"^(?P<pk>\d+)/admin/update/$",
        view=AppAdminUpdateView.as_view(),
        name="apps.update.administrator",
    ),
    re_path(
        r"^(?P<pk>\d+)/superuser/update/$",
        view=AppSuperUpdateView.as_view(),
        name="apps.update.superuser",
    ),
    re_path(
        r"^app/group/(?P<pk>\d+)/delete/$",
        view=AppGroupDeleteView.as_view(),
        name="apps.group.delete",
    ),
    re_path(
        r"^app/group/(?P<pk>\d+)/detail/$",
        view=AppGroupDetailView.as_view(),
        name="apps.group.detail",
    ),
    re_path(
        r"^app/group/(?P<slug>[-\w\d]+)/$",
        view=AppGroupListView.as_view(),
        name="apps.group.list",
    ),
    re_path(
        r"^app/group/(?P<slug>[-\w\d]+)/create/$",
        view=AppGroupCreateView.as_view(),
        name="apps.group.create",
    ),
    re_path(
        r"^app/group/(?P<pk>\d+)/update/$",
        view=AppGroupUpdateView.as_view(),
        name="apps.group.update",
    ),
    re_path(r"^group/$", view=GroupListView.as_view(), name="group.list"),
    re_path(
        r"^group/create/$",
        view=GroupCreateView.as_view(),
        name="group.create",
    ),
    re_path(
        r"^group/(?P<pk>\d+)/$",
        view=GroupDetailListView.as_view(),
        name="group.detail",
    ),
    re_path(
        r"^group/(?P<pk>\d+)/update/$",
        view=GroupUpdateView.as_view(),
        name="group.update",
    ),
]
