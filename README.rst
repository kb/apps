Applications
************

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-apps
  # or
  python3 -m venv venv-apps

  source venv-apps/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

Release
=======

https://www.kbsoftware.co.uk/docs/
