# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path
from rest_framework import routers
from rest_framework.authtoken import views

from apps.api import AppViewSet, AppGroupUserViewSet, AppGroupViewSet
from .views import (
    ContactEditAccountsView,
    ContactEditCrmView,
    DashAccountsView,
    DashCrmView,
    DashView,
    ExampleAppBaseMixinView,
    HomeView,
    SettingsView,
    UserDetailView,
)


router = routers.DefaultRouter()  # trailing_slash=False)
router.register(r"apps", AppViewSet, basename="app")
router.register(r"appgroups", AppGroupViewSet, basename="appgroup")
router.register(r"appgroupusers", AppGroupUserViewSet, basename="appgroupuser")


urlpatterns = [
    re_path(r"^api/0.1/", view=include((router.urls, "api"), namespace="api")),
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(
        r"^contact/edit/accounts/$",
        view=ContactEditAccountsView.as_view(),
        name="example.contact.edit.accounts",
    ),
    re_path(
        r"^contact/edit/crm/$",
        view=ContactEditCrmView.as_view(),
        name="example.contact.edit.crm",
    ),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(
        r"^dash/accounts/$",
        view=DashAccountsView.as_view(),
        name="example.dash.accounts",
    ),
    re_path(
        r"^dash/crm/$",
        view=DashCrmView.as_view(),
        name="example.dash.crm",
    ),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    re_path(r"^apps/", view=include("apps.urls")),
    re_path(
        r"^example/app/base/mixin/",
        view=ExampleAppBaseMixinView.as_view(),
        name="example.app.base.mixin",
    ),
    re_path(r"^mail/", view=include("mail.urls")),
    re_path(
        r"^user/(?P<pk>\d+)/$",
        view=UserDetailView.as_view(),
        name="project.user.detail",
    ),
    re_path(r"^token/$", view=views.obtain_auth_token, name="api.token.auth"),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
