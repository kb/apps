# -*- encoding: utf-8 -*-
import pytest

from apps.models import App
from apps.tests.factories import (
    AppFactory,
    AppGroupFactory,
    AppGroupUserFactory,
    AppSettingsFactory,
)
from example_apps.models import MyApps
from login.tests.factories import GroupFactory, UserFactory


@pytest.mark.django_db
def test_has_access_administrator(django_assert_num_queries):
    """Does the user have access to an app.

    For documentation, see:
    https://www.kbsoftware.co.uk/docs/app-apps.html#permissions

    """
    user = UserFactory()
    app = AppFactory(slug=MyApps.CRM)
    app.administrators.add(user)
    with django_assert_num_queries(2):
        assert App.objects.has_access(user, MyApps.CRM) is True


@pytest.mark.django_db
def test_has_access_app_group(django_assert_num_queries):
    user = UserFactory()
    app = AppFactory(slug=MyApps.CRM)
    app_group = AppGroupFactory(app=AppFactory(slug="another-app"))
    app.app_group.add(app_group)
    AppGroupUserFactory(app_group=app_group, user=user)
    with django_assert_num_queries(2):
        assert App.objects.has_access(user, MyApps.CRM) is True


@pytest.mark.django_db
def test_has_access_global_group(django_assert_num_queries):
    user = UserFactory()
    group = GroupFactory()
    user.groups.add(group)
    app = AppFactory(slug=MyApps.CRM)
    app.global_group.add(group)
    with django_assert_num_queries(2):
        assert App.objects.has_access(user, MyApps.CRM) is True


@pytest.mark.django_db
def test_has_access_not(django_assert_num_queries):
    user = UserFactory()
    AppFactory(slug=MyApps.CRM)
    with django_assert_num_queries(2):
        assert App.objects.has_access(user, MyApps.CRM) is False


@pytest.mark.django_db
def test_has_access_superuser(django_assert_num_queries):
    user = UserFactory(is_superuser=True)
    app = AppFactory(slug=MyApps.CRM)
    with django_assert_num_queries(1):
        assert App.objects.has_access(user, MyApps.CRM) is True


@pytest.mark.django_db
def test_has_access_superuser_application(django_assert_num_queries):
    user = UserFactory()
    app_settings = AppSettingsFactory()
    app_settings.app_superusers.add(user)
    AppFactory(slug=MyApps.CRM)
    with django_assert_num_queries(2):
        assert App.objects.has_access(user, MyApps.CRM) is True
