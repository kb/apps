# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from apps.models import AppError
from apps.tests.factories import AppFactory
from example_apps.models import MyApps
from login.tests.factories import GroupFactory, TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_example_app_base_mixin(client):
    user = UserFactory()
    app = AppFactory(
        slug=MyApps.CRM, url_dash="https://www.kbsoftware.co.uk/dash/"
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("example.app.base.mixin")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "app_menu" in response.context
    app_menu = response.context["app_menu"]
    assert {
        "current_app": app,
        "current_app_admin": False,
        "show_app_menu": False,
        "show_settings": False,
    } == app_menu


@pytest.mark.django_db
def test_example_app_base_mixin_administrator(client):
    user = UserFactory()
    app = AppFactory(
        slug=MyApps.CRM, url_dash="https://www.kbsoftware.co.uk/dash/"
    )
    app.administrators.add(user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("example.app.base.mixin")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "app_menu" in response.context
    app_menu = response.context["app_menu"]
    assert {
        "current_app": app,
        "current_app_admin": True,
        "show_app_menu": False,
        "show_settings": True,
    } == app_menu


@pytest.mark.django_db
def test_example_app_base_mixin_missing_app(client):
    user = UserFactory()
    AppFactory(slug="another")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("example.app.base.mixin")
    with pytest.raises(AppError) as e:
        client.get(url)
    assert "Cannot find an 'App' with a slug of 'crm'" in str(e.value)


@pytest.mark.django_db
def test_example_app_base_mixin_two_apps(client):
    user = UserFactory()
    group = GroupFactory()
    user.groups.add(group)
    # app1
    app1 = AppFactory(
        slug=MyApps.CRM, url_dash="https://www.kbsoftware.co.uk/dash/"
    )
    app1.global_group.add(group)
    # app2
    app2 = AppFactory(
        slug="fruit", url_dash="https://www.kbsoftware.co.uk/dash/"
    )
    app2.global_group.add(group)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("example.app.base.mixin")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "app_menu" in response.context
    app_menu = response.context["app_menu"]
    assert {
        "current_app": app1,
        "current_app_admin": False,
        "show_app_menu": True,
        "show_settings": False,
    } == app_menu


@pytest.mark.django_db
def test_example_app_settings(client):
    """Check the settings menu appears when we have no current app.

    The settings view does not have a 'current_app_slug', but if we have two
    apps, and I am an app administrator for one of them, I should see the
    settings menu.

    """
    user = UserFactory()
    group = GroupFactory()
    user.groups.add(group)
    # app1
    app1 = AppFactory()
    app1.administrators.add(user)
    # app2
    app2 = AppFactory()
    app2.global_group.add(group)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("project.settings")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "app_menu" in response.context
    app_menu = response.context["app_menu"]
    assert {
        "current_app": None,
        "current_app_admin": False,
        "show_app_menu": True,
        "show_settings": True,
    } == app_menu
