# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.views.generic import DetailView, TemplateView

from apps.views import is_contact_or_app_admin, SettingsBaseMixin
from base.view_utils import BaseMixin
from example_apps.models import MyApps


class ContactEditAccountsView(LoginRequiredMixin, BaseMixin, TemplateView):
    current_app_slug = MyApps.ACCOUNTS
    template_name = "example/example.html"


class ContactEditCrmView(LoginRequiredMixin, BaseMixin, TemplateView):
    current_app_slug = MyApps.CRM
    template_name = "example/example.html"


class DashAccountsView(LoginRequiredMixin, BaseMixin, TemplateView):
    current_app_slug = MyApps.ACCOUNTS
    template_name = "example/dash.html"


class DashCrmView(LoginRequiredMixin, BaseMixin, TemplateView):
    current_app_slug = MyApps.CRM
    template_name = "example/dash.html"


class DashView(LoginRequiredMixin, BaseMixin, TemplateView):
    template_name = "example/dash.html"


class ExampleAppBaseMixinView(LoginRequiredMixin, BaseMixin, TemplateView):
    current_app_slug = MyApps.CRM
    template_name = "example/example.html"


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class SettingsView(
    LoginRequiredMixin, UserPassesTestMixin, SettingsBaseMixin, TemplateView
):
    template_name = "example/settings.html"

    def test_func(self, user):
        return is_contact_or_app_admin(user)


class UserDetailView(LoginRequiredMixin, BaseMixin, DetailView):
    model = User
